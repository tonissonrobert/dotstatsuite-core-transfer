﻿using DotStat.Common.Auth;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Db;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Utils;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Exception;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.MetadataStructure;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.DataParser.Factory;
using Org.Sdmxsource.Sdmx.DataParser.Manager;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data;
using Org.Sdmxsource.Util.Io;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Dataflow = DotStat.Domain.Dataflow;

namespace DotStat.Transfer.Producer
{
    public abstract class BaseFileProducer<T> : IProducer<T> where T : TransferParam, ITransferParamWithFilePath
    {
        protected readonly IAuthorizationManagement AuthorizationManagement;
        protected IMappingStoreDataAccess DataAccess { get; }
        protected ReadableDataLocationFactory DataLocationFactory { get; }
        private readonly ITempFileManagerBase _tempFileManager;

        protected BaseFileProducer(
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess dataAccess,
            ITempFileManagerBase tempFileManager)
        {
            AuthorizationManagement = authorizationManagement;
            DataAccess = dataAccess;
            DataLocationFactory = new ReadableDataLocationFactory();
            _tempFileManager = tempFileManager;
        }

        public bool IsAuthorized(T transferParam, IImportReferenceableStructure referencedStructure)
        {
            //As the Source is a file there is no need to check if the User can read Data.
            return true;
        }

        public async Task<IImportReferenceableStructure> GetReferencedStructure(T transferParam, bool throwErrorIfNotFound = true)
        {
            await InitInternals(transferParam);

            return transferParam.ReferencedStructure ?? throw new DotStatException(
                LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.NoDataflowReferenceInputDataset,
                    transferParam.CultureInfo.TwoLetterISOLanguageName
                ));
        }

        public async Task<TransferContent> Process(T transferParam, IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
            {
                throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound), transferParam.FilePath));
            }

            var canMergeData = IsAuthorized(StagingRowActionEnum.Merge, transferParam, referencedStructure);
            var canReplaceData = IsAuthorized(StagingRowActionEnum.Replace, transferParam, referencedStructure);
            var canDeleteData = IsAuthorized(StagingRowActionEnum.Delete, transferParam, referencedStructure);

            if (!canMergeData && !canDeleteData)
            {
                throw new UnauthorizedAccessException();
            }

            try
            {
                await InitInternals(transferParam);

                var transferContent = new TransferContent
                {
                    ReportedComponents = transferParam.ReportedComponents,
                    DatasetAttributes = new List<DataSetAttributeRow>(),
                    IsXMLSource = transferParam.IsXmlSource ?? throw new ArgumentNullException(nameof(transferParam.IsXmlSource)) // InitInternals method sets this transferParam property
                };

                if (transferParam.TransferType == TransferType.MetadataOnly)
                {
                    transferContent.MetadataObservations = ProcessSdmxFile(transferParam.FilePath, transferParam.DestinationDataspace.Id, referencedStructure, transferContent.DatasetAttributes, canMergeData, canDeleteData, canReplaceData, cancellationToken);
                }
                else
                {
                    transferContent.DataObservations = ProcessSdmxFile(transferParam.FilePath, transferParam.DestinationDataspace.Id, referencedStructure, transferContent.DatasetAttributes, canMergeData, canDeleteData, canReplaceData, cancellationToken);
                }

                return transferContent;
            }
            catch (SdmxNotImplementedException ex)
            {
                throw new TransferFailedException(ex.Message);
            }
        }

        protected async IAsyncEnumerable<ObservationRow> ProcessSdmxFile(string filename, string dataspace, IImportReferenceableStructure referencedStructure, IList<DataSetAttributeRow> datasetAttributes, bool canMerge, bool canDelete, bool canReplaceData, CancellationToken cancellation)
        {
            using var sourceData = DataLocationFactory.GetReadableDataLocation(new FileInfo(filename));
          
            var sdmxObjectRetrievalManager = DataAccess.GetRetrievalManager(dataspace);
            using var dataReaderEngine = referencedStructure is Dataflow dataFlow
                ? new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData,
                    referencedStructure.Dsd.Base, dataFlow?.Base, referencedStructure.Dsd.Msd?.Base)
                : new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData,
                    sdmxObjectRetrievalManager);
                

            //Not valid SDMX content
            if (dataReaderEngine is null)
                throw new SdmxNotImplementedException();

            foreach (var observationRow in await Task.Run(() => ReadSdmxFile(dataReaderEngine, referencedStructure.Dsd, datasetAttributes, canMerge, canDelete, canReplaceData, cancellation), cancellation))
            {
                cancellation.ThrowIfCancellationRequested();
                yield return observationRow;
            }
        }

        private static IEnumerable<ObservationRow> ReadSdmxFile(IDataReaderEngine reader, IImportReferenceableStructure referencedStructure, IList<DataSetAttributeRow> datasetAttributes, bool canMergeData, bool canDeleteData, bool canReplaceData, CancellationToken cancellation)
        {
            var dataFlow = reader.Dataflow is null? null : new Dataflow(reader.Dataflow, referencedStructure.Dsd);

            var groupAttributes = new Dictionary<HashSet<string>, List<IKeyValue>>();
            var nonTimeDimensions = referencedStructure.Dsd.Dimensions.Where(d => !d.Base.TimeDimension).ToList();
            var timeDimension = referencedStructure.Dsd.Base.TimeDimension?.Id;
            var hasTimeDimension = !string.IsNullOrEmpty(timeDimension);
            var finishedProcessingGroups = false;

            var isXml = reader.IsXml();
            var checkAuthorizedEveryLevel = !isXml;
            var isActionSameForAllObservations = isXml; // In xml data files the action is the same for the entire dataset

            var dataSetNumber = 1;
            var batchNumber = 1;
            var rowNumber = 1;

            //DataSet Level 
            while (reader.MoveNextDataset())
            {
                cancellation.ThrowIfCancellationRequested();
                var currentAction = reader.GetStagingRowActionEnum();
                currentAction.CheckAuthorized(canMergeData, canDeleteData, canReplaceData, dataSetNumber, isXml);

                StagingRowActionEnum previousAction = currentAction;
                var previousCoordinateWildcarded = false;
                var isFirstStagingRow = true;

                //XML reader reports DataSet attributes only once at DataSet level
                //CSV reader reports DataSet attributes every observation
                if (reader.IsXml())
                {
                    if (reader.DatasetAttributes.Any() && currentAction != StagingRowActionEnum.Delete)
                    {
                        var dataSetAttributeRow = new DataSetAttributeRow(batchNumber, currentAction);

                        foreach (var dataSetAttribute in reader.DatasetAttributes)
                        {
                            dataSetAttributeRow.Attributes.Add(dataSetAttribute);
                        }

                        datasetAttributes.Add(dataSetAttributeRow);
                    }
                }

                //Group/Series Level
                if (reader.MoveNextKeyable())
                {
                    do
                    {
                        cancellation.ThrowIfCancellationRequested();
                        if (checkAuthorizedEveryLevel)
                        {
                            reader.GetStagingRowActionEnum().CheckAuthorized(canMergeData, canDeleteData, canReplaceData, dataSetNumber, isXml);
                        }

                        //group found after processing series
                        if (finishedProcessingGroups && !reader.CurrentKey.Series)
                            throw new DotStatException(
                                LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                    .SDMXMLErrorGropsAfterSeries));

                        var currentKeyable = reader.CurrentKey;

                        var timeKeyValue = hasTimeDimension
                            ? reader.CurrentKey.Key.FirstOrDefault(k =>
                                k.Concept.Equals(timeDimension, StringComparison.CurrentCultureIgnoreCase))
                            : null;
                        var obsTime = timeKeyValue?.Code;

                        var currentKeyableCoordinate =
                            new HashSet<string>(currentKeyable?.Key?.Select(k => $"{k.Concept}:{k.Code}"));

                        //Process group keys
                        if (!reader.CurrentKey.Series)
                        {
                            groupAttributes.Add(currentKeyableCoordinate, currentKeyable?.Attributes?.ToList());
                            continue;
                        }
                        //Process series keys and observations

                        finishedProcessingGroups = true;

                        var currentCoordinateWildCarded = currentKeyable.Key.Any(kv => string.IsNullOrWhiteSpace(kv.Code))
                                                          || currentKeyable.Key.Any(kv => kv.Code.Equals(DbExtensions.DimensionWildCarded))
                                                          || nonTimeDimensions.Any(d => !currentKeyable.Key.Select(d => d.Concept).Contains(d.Base.Id));//missing reported dimensions

                        //add series attributes
                        var seriesAttributes = new List<IKeyValue>();
                        if (currentKeyable is not null && currentKeyable.Attributes.Any())
                            seriesAttributes.AddRange(currentKeyable.Attributes);

                        //add group attributes (merge do not have no time dim ref, deletions can have dim ref at seriesKey)
                        var dimGroupAttributesWithNoTimeDim = groupAttributes
                            .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentKeyableCoordinate))
                            .SelectMany(item => item.Value).ToList();

                        seriesAttributes.AddRange(dimGroupAttributesWithNoTimeDim);

                        //Observation Level 
                        if (reader.MoveNextObservation())
                        {
                            do
                            {
                                cancellation.ThrowIfCancellationRequested();
                                if (!isActionSameForAllObservations)
                                {
                                    currentAction = reader.GetStagingRowActionEnum();
                                }

                                if (checkAuthorizedEveryLevel)
                                {
                                    currentAction.CheckAuthorized(canMergeData, canDeleteData, canReplaceData, rowNumber, isXml);
                                }

                                var currentObservation = reader.CurrentObservation;
                                obsTime = currentObservation?.ObsTime ?? obsTime;
                                var timeDimensionOmitted = hasTimeDimension && string.IsNullOrEmpty(obsTime);

                                if (!isFirstStagingRow && // No need to increase batch no. for the first staging row
                                    (currentCoordinateWildCarded || timeDimensionOmitted || // Each wildcarded row should go into a new batch
                                     previousCoordinateWildcarded || // Start new batch if prev coordinate was wildcarded but not the current
                                     (previousAction != currentAction))) // Start new batch if action changes
                                {
                                    batchNumber++;
                                }

                                //Include group attributes referencing the time dimension
                                var dimGroupAttributesWithTimeDim = new List<IKeyValue>();
                                if (hasTimeDimension)
                                {
                                    var currentObsCoordinate = currentObservation is null ?
                                        new HashSet<string>() : new HashSet<string>(
                                            currentObservation.SeriesKey?.Key?.Select(k => $"{k.Concept}:{k.Code}"));

                                    dimGroupAttributesWithTimeDim = groupAttributes
                                        .Where(dimGroupSerie => dimGroupSerie.Key.IsSubsetOf(currentObsCoordinate))
                                        .SelectMany(item => item.Value)
                                        .Where(attribute => attribute.Concept.Equals(timeDimension, StringComparison.CurrentCultureIgnoreCase))
                                        .ToList();
                                }

                                var allAttributes = new List<IKeyValue>();

                                //CSV readers modify the value of the dataset level attributes, every row
                                if (!reader.IsXml())
                                {
                                    //add dataSet Level attributes
                                    allAttributes.AddRange(
                                        reader.DatasetAttributes);
                                }
                                //Force to push XML dataSet level attributes to obs level when the action is Delete
                                //To keep track of deletions for the updatedAfter feature
                                else if (currentAction == StagingRowActionEnum.Delete && reader.DatasetAttributes.Any())
                                {
                                    allAttributes.AddRange(reader.DatasetAttributes.Select(dataSetAttribute => string.IsNullOrEmpty(dataSetAttribute.Code)
                                        ? new KeyValueImpl(DbExtensions.ColumnPresentDbValue.ToString(), dataSetAttribute.Concept)
                                        : dataSetAttribute));
                                }

                                //add series Level attributes
                                allAttributes.AddRange(seriesAttributes);

                                //add group attributes with  time dim ref
                                if (dimGroupAttributesWithTimeDim.Any())
                                    allAttributes.AddRange(dimGroupAttributesWithTimeDim);

                                //add obs attributes
                                if (currentObservation is not null && currentObservation.Attributes.Any())
                                {
                                    allAttributes.AddRange(currentObservation.Attributes);
                                }

                                //TODO: We could use a lighter object (Not IObservation) to improve performance
                                yield return new ObservationRow(
                                    batchNumber,
                                    currentAction,
                                    new ObservationImpl(currentKeyable, obsTime, currentObservation?.ObservationValue, allAttributes, crossSectionValue: null),
                                    currentCoordinateWildCarded || timeDimensionOmitted);

                                rowNumber++;

                                isFirstStagingRow = false;
                                previousCoordinateWildcarded = currentCoordinateWildCarded || timeDimensionOmitted;
                                previousAction = currentAction;

                            } while (reader.MoveNextObservation());
                        }
                        else
                        {
                            var timeDimensionOmitted = hasTimeDimension && string.IsNullOrEmpty(obsTime);

                            if (!isFirstStagingRow && // No need to increase batch no. for the first staging row
                                (currentCoordinateWildCarded || timeDimensionOmitted || // Each wildcarded row should go into a new batch
                                 previousCoordinateWildcarded || // Start new batch if prev coordinate was wildcarded but not the current
                                 (previousAction != currentAction))) // Start new batch if action changes
                            {
                                batchNumber++;
                            }

                            yield return new ObservationRow(
                                batchNumber,
                                currentAction,
                                new ObservationImpl(currentKeyable, obsTime, null, seriesAttributes, crossSectionValue: null),
                                currentCoordinateWildCarded || timeDimensionOmitted
                            );

                            previousCoordinateWildcarded = currentCoordinateWildCarded;
                            previousAction = currentAction;
                        }

                        isFirstStagingRow = false;

                    } while (reader.MoveNextKeyable());
                }
                else
                {
                    // In XML data messages there could be dataset level attributes provided without any keyables/observations
                    if (currentAction is StagingRowActionEnum.Delete)
                    {
                        // All observation and series level components have been omitted, therefore no need to check if all components are present
                        // If no dimensions are reported at dataset level, then at this stage it means "DeleteAll"
                        if (!reader.DatasetAttributes.Any())
                        {
                            yield return new ObservationRow(batchNumber: batchNumber, action: StagingRowActionEnum.DeleteAll, observation: null, isWildCarded: true);
                        }
                        else if (reader.IsXml())
                        {
                            yield return new ObservationRow(
                                batchNumber,
                                currentAction,
                                new ObservationImpl(new KeyableImpl(dataFlow?.Base, referencedStructure.Dsd.Base, new List<IKeyValue>(), null), null, null, reader.DatasetAttributes, crossSectionValue: null), isWildCarded: true);
                        }
                    }

                    batchNumber++;
                }

                dataSetNumber++;
            }
        }
        
        public void Dispose()
        {
            //Nothing to do, IDataReaderEngine is within a "using" clause
        }

        protected bool IsAuthorized(StagingRowActionEnum action, T transferParam, IImportReferenceableStructure referencedStructure)
        {
            return action switch
            {
                StagingRowActionEnum.Merge => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, referencedStructure.AgencyId, referencedStructure.Code,
                    referencedStructure.Version.ToString(), PermissionType.CanImportData) || AuthorizationManagement.IsAuthorized(
                    transferParam.Principal, transferParam.DestinationDataspace.Id, referencedStructure.AgencyId, referencedStructure.Code,
                    referencedStructure.Version.ToString(), PermissionType.CanUpdateData),
                StagingRowActionEnum.Replace => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, referencedStructure.AgencyId, referencedStructure.Code,
                    referencedStructure.Version.ToString(), PermissionType.CanImportData) || AuthorizationManagement.IsAuthorized(
                    transferParam.Principal, transferParam.DestinationDataspace.Id, referencedStructure.AgencyId, referencedStructure.Code,
                    referencedStructure.Version.ToString(), PermissionType.CanUpdateData),
                StagingRowActionEnum.Delete => AuthorizationManagement.IsAuthorized(transferParam.Principal,
                    transferParam.DestinationDataspace.Id, referencedStructure.AgencyId, referencedStructure.Code,
                    referencedStructure.Version.ToString(), PermissionType.CanDeleteData),
                _ => false
            };
        }

        protected async Task InitInternals(T transferParam)
        {
            if (transferParam.IsInited)
                return;

            await GetFileFromSource(transferParam);

            try
            {
                var sdmxObjectRetrievalManager = DataAccess.GetRetrievalManager(transferParam.DestinationDataspace.Id);
                using var sourceData = DataLocationFactory.GetReadableDataLocation(new FileInfo(transferParam.FilePath));
                using var dataReaderEngine = new SdmxDataReaderFactory(new DataInformationManager(), null).GetDataReaderEngine(sourceData, sdmxObjectRetrievalManager)
                                             ?? throw new SdmxNotImplementedException();

                if (!dataReaderEngine.MoveNextDataset())
                {
                    return;
                }

                IMetadataStructureDefinitionObject metadataStructureObject = null;
                if (dataReaderEngine is CsvDataReaderEngineV2 { MetadataStructure: { } } csvDataReaderEngineV2)
                {
                    metadataStructureObject = csvDataReaderEngineV2.MetadataStructure;
                }

                if (dataReaderEngine.Dataflow == null && dataReaderEngine.DataStructure == null)
                {
                    throw new DotStatException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoDataflowReferenceInputDataset, transferParam.CultureInfo.TwoLetterISOLanguageName));
                }

                var dataFlow = dataReaderEngine.Dataflow == null ? null :
                    DataAccess.GetDataflow(transferParam.DestinationDataspace.Id,
                    dataReaderEngine.Dataflow,
                    dataReaderEngine.DataStructure,
                    metadataStructureObject);

                var dsd = dataFlow is not null ? dataFlow.Dsd :
                    DataAccess.GetDsd(transferParam.DestinationDataspace.Id,
                        dataReaderEngine.DataStructure,
                        metadataStructureObject);

                IImportReferenceableStructure referencedStructure = dataFlow == null ? dsd : dataFlow;

                // Reported Components -----------------------------
                var reportedComponents = new ReportedComponents()
                {
                    CultureInfo = transferParam.CultureInfo
                };

                transferParam.IsXmlSource = dataReaderEngine.IsXml();

                //In XML there is no way to know in advance which components are reported, therefore we mark all measures and attributes as reported
                //In XML we do not support referential metadata
                if (transferParam.IsXmlSource.Value)
                {
                    reportedComponents.Dimensions = referencedStructure.Dsd.Dimensions.ToList();
                    reportedComponents.TimeDimension = referencedStructure.Dsd.TimeDimension;
                    reportedComponents.DatasetAttributes = referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList();

                    //attributes not attached to the time dimension
                    reportedComponents.SeriesAttributesWithNoTimeDim = referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                        .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                        .ToList();

                    //attributes attached to the time dimension
                    reportedComponents.ObservationAttributes = referencedStructure.Dsd.Attributes
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                                    || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

                    reportedComponents.IsPrimaryMeasureReported = true;
                }
                // CSV readers
                else if (dataReaderEngine.MoveNextKeyable() && dataReaderEngine.MoveNextObservation())
                {
                    var currentObservation = dataReaderEngine.CurrentObservation;

                    //Dimensions' found in csv
                    reportedComponents.Dimensions = referencedStructure.Dsd.Dimensions
                        .Where(d => currentObservation.SeriesKey.Key.Any(sk => sk.Concept.Equals(d.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

                    //Time dimension found in csv
                    if (currentObservation.ObsTime != null)
                    {
                        reportedComponents.TimeDimension = referencedStructure.Dsd.TimeDimension;
                        reportedComponents.Dimensions.Add(referencedStructure.Dsd.TimeDimension);
                    }

                    //Measure column found in csv
                    if (currentObservation.ObservationValue != null)
                    {
                        reportedComponents.IsPrimaryMeasureReported = true;
                    }

                    //Dataset attributes' columns found in csv
                    reportedComponents.DatasetAttributes = referencedStructure.Dsd.Attributes
                        .Where(a => dataReaderEngine.DatasetAttributes.Any(ar =>
                            ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase))).ToList();

                    //Series attributes' columns found in csv (attributes not attached to the time dimension)
                    reportedComponents.SeriesAttributesWithNoTimeDim = referencedStructure.Dsd.Attributes
                            .Where(a => currentObservation.Attributes.Any(ar => ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                            .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                            .ToList();

                    //Observation attributes' columns found in csv (attributes attached to the time dimension)
                    reportedComponents.ObservationAttributes = referencedStructure.Dsd.Attributes
                        .Where(a => currentObservation.Attributes.Any(ar => ar.Concept.Equals(a.Code, StringComparison.InvariantCultureIgnoreCase)))
                        .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                        || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList();

                    if (dataReaderEngine is CsvDataReaderEngineV2 && referencedStructure.Dsd.Msd is not null)
                    {
                        var metadataAttributes = referencedStructure.Dsd.Msd.MetadataAttributes
                            .Where(x => currentObservation.Attributes.Any(ar =>
                                ar.Concept.Equals(x.Base.GetFullIdPath(false), StringComparison.InvariantCultureIgnoreCase)));

                        reportedComponents.MetadataAttributes = metadataAttributes
                            .Select(x => new MetadataAttribute(x.Base) { Dsd = referencedStructure.Dsd, HierarchicalId = x.Base.GetFullIdPath(false) })
                            .ToList();

                        if (!reportedComponents.IsDataReported() && reportedComponents.IsMetadataReported())
                        {
                            transferParam.TransferType = TransferType.MetadataOnly;
                        }
                    }

                }

                transferParam.ReferencedStructure = referencedStructure;
                transferParam.ReportedComponents = reportedComponents;
                transferParam.IsInited = true;

                // -------------------------------------------------
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvInvalidDelimiter))
            {
                throw new SdmxSemmanticException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvInvalidDelimiter), e);
            }
            catch (SdmxSemmanticException e) when (Equals(e.Code, ExceptionCode.SdmxCsvMissingDimensions))
            {
                if (e.InnerException != null && !string.IsNullOrEmpty(e.InnerException.Message))
                {
                    var dimensionAndAttributeList = e.InnerException.Message;
                    throw new SdmxSemmanticException(string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CsvMissingDimensions),
                            dimensionAndAttributeList),
                        e);
                }

                throw;
            }
        }
        
        protected virtual async Task GetFileFromSource(T transferParam)
        {
            var fileInfo = new FileInfo(transferParam.FilePath);

            if (!fileInfo.Exists)
            {
                throw new DotStatException(string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileNotFound), transferParam.FilePath));
            }

            if (transferParam.IsZipProcessed ||
                (!(transferParam.FilePath ?? string.Empty).EndsWith(".zip", StringComparison.OrdinalIgnoreCase) &&
                 !(transferParam.DataSource ?? string.Empty).EndsWith(".zip", StringComparison.OrdinalIgnoreCase)))
            {
                return;
            }
            var tempFile = _tempFileManager.GetTempFileName(fileInfo.Name + ".unzipped");

            // unzip only once
            if (!File.Exists(tempFile))
            {
                using (var zip = ZipFile.OpenRead(fileInfo.FullName))
                {
                    var entry = zip.Entries.First();
                    entry.ExtractToFile(tempFile, true);
                }

                File.SetCreationTime(tempFile, DateTime.Now);
            }

            if (transferParam.FilePath != tempFile)
            {
                transferParam.FilesToDelete.Add(tempFile);
                transferParam.FilePath = tempFile;
            }

            transferParam.IsZipProcessed = true;

            await Task.CompletedTask;
        }

    }
}
