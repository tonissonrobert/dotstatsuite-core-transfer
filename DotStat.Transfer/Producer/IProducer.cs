﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;
using DotStat.Transfer.ImportReferencedStructureManager;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.Producer
{
    public interface IProducer<in T>: IImportReferencedStructureManager<T>, IDisposable where T : ITransferParam
    {
        Task<TransferContent> Process(T transferParam, IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken);

        bool IsAuthorized(T transferParam, IImportReferenceableStructure referencedStructure);
    }
}