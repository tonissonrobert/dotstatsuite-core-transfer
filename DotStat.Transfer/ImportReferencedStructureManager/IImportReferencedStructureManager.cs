﻿using System.Threading.Tasks;
using DotStat.Domain;
using DotStat.Transfer.Param;

namespace DotStat.Transfer.ImportReferencedStructureManager
{
    public interface IImportReferencedStructureManager<in T> where T : ITransferParam 
    {
        Task<IImportReferenceableStructure> GetReferencedStructure(T transferParam, bool throwErrorIfNotFound = true);
    }

}