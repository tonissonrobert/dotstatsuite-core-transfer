﻿
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class ConcurrentTransactionException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException()
        {
        }

        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public ConcurrentTransactionException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
