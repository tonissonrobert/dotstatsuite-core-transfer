﻿
using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Exceptions;

namespace DotStat.Transfer.Exception
{
    public class DataflowInitializationFailedException : DotStatException
    {
        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException()
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException(string message) : base(message)
        {
        }

        [ExcludeFromCodeCoverage]
        public DataflowInitializationFailedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
