using DotStat.Common.Configuration;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Manager
{
    public class SdmxUrlToSqlTransferManager : UrlTransferManager<SdmxUrlToSqlTransferParam>
    {
        public SdmxUrlToSqlTransferManager(BaseConfiguration configuration, IProducer<SdmxUrlToSqlTransferParam> observationProducer, IConsumer<SdmxUrlToSqlTransferParam> observationConsumer) 
            : base(configuration, observationProducer, observationConsumer)
        {
        }
    }
}
