﻿using DotStat.Domain;

namespace DotStat.Transfer.Processor
{
    public interface ITransferProcessor
    {
        TransferContent Process(IImportReferenceableStructure sourceReferencedStructure, IImportReferenceableStructure destinationReferencedStructure, TransferContent transferContent);
    }
}
