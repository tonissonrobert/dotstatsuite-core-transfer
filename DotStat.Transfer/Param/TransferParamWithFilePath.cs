﻿using DotStat.Common.Configuration.Dto;
using DotStat.Domain;
using DotStat.Transfer.Model;
using DotStat.Transfer.Utils;
using System.Globalization;

namespace DotStat.Transfer.Param
{
    public class TransferParamWithFilePath : TransferParam, ITransferParamWithFilePath
    {
        public string FilePath { get; set; }
        public bool IsZipProcessed { get; set; }

        public TransferParamWithFilePath(InternalSdmxParameters internalSdmxParameters, DataspaceInternal destinationDataspace, string language)
        {
            DestinationDataspace = destinationDataspace;
            CultureInfo = CultureInfo.GetCultureInfo(language);
            ValidationType = internalSdmxParameters.ImportValidationType;
            PITReleaseDate = internalSdmxParameters.PITReleaseDate.GetPITReleaseDate(language);

            if (internalSdmxParameters.targetVersion != null)
            {
                TargetVersion = (TargetVersion)internalSdmxParameters.targetVersion;
            }

            if (internalSdmxParameters.restorationOptionRequired != null)
            {
                PITRestorationAllowed = (bool)internalSdmxParameters.restorationOptionRequired;
            }

            if (string.IsNullOrEmpty(internalSdmxParameters.filepath))
            {
                internalSdmxParameters.FileLocalPath.TempLocalFileExists("SDMX", language);
                FilePath = internalSdmxParameters.FileLocalPath;
                DataSource = internalSdmxParameters.OriginalFileName;

                FilesToDelete.Add(internalSdmxParameters.FileLocalPath);
            }
            else
            {
                FilePath = internalSdmxParameters.filepath;
                DataSource = internalSdmxParameters.filepath;
            }
        }
    }
}
