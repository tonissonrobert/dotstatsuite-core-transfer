﻿namespace DotStat.Transfer.Param
{
    public class ExcelToSqlTransferParam : TransferParam
    {
        public string ExcelFilePath { get; set; }
        public string EddFilePath { get; set; }
    }
}
