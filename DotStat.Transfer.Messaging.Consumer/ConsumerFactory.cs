﻿using System.Diagnostics.CodeAnalysis;
using DotStat.Common.Messaging;
using DotStat.Transfer.Interface;
using Microsoft.Extensions.Configuration;
using System;
using Microsoft.Extensions.DependencyInjection;
using DotStat.Transfer.Manager;
using DotStat.Common.Logger;
using DotStat.Db.Service;
using DotStat.MappingStore;

namespace DotStat.Transfer.Messaging.Consumer
{
    [ExcludeFromCodeCoverage]
    public class ConsumerFactory : IConsumerFactory
    {

        public IConsumer Create(IServiceProvider serviceProvider, IConfiguration configuration, IMailService mailService, DotStatDbServiceResolver dotStatDbServiceResolver)
        {
            try
            {
                MessagingServiceConfiguration messsageBrokerConfiguration = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();

                if (messsageBrokerConfiguration?.Enabled == true)
                {
                    using (IServiceScope scope = serviceProvider.CreateScope())
                    {
                        var mappingStoreDataAccess = scope.ServiceProvider.GetRequiredService<IMappingStoreDataAccess>();
                        var commonMngr = scope.ServiceProvider.GetRequiredService<ICommonManager>();

                        return new RabbitMqConsumer(configuration, commonMngr, mappingStoreDataAccess, mailService, dotStatDbServiceResolver);
                    }
                }
            }
            catch (System.Exception e)
            {
                Log.Fatal(e);
            }

            return null;
        }
    }
}
