﻿using System;

namespace DotStat.Transfer.Messaging.Consumer
{
    public interface IConsumer : IDisposable
    {
        void Start();
    }
}
