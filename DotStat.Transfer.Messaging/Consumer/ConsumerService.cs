﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.Extensions.Hosting;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Db.Manager;
using DotStat.MappingStore;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using log4net;
using Microsoft.Extensions.Configuration;

namespace DotStat.Transfer.Messaging.Consumer
{
    [ExcludeFromCodeCoverage]
    public class ConsumerService :  BackgroundService 
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(ConsumerService));

        private IConsumer _consumer;
        private readonly IConsumerFactory _consumerFactory;
        private readonly IConfiguration _configuration;
        private readonly IDbManager _dbManager;
        private readonly CommonManager _commonMngr;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IMailService _mailService;

        [ExcludeFromCodeCoverage]
        public ConsumerService(IConfiguration configuration, IConsumerFactory consumerFactory, CommonManager commonMngr, IDbManager dbManager, IMappingStoreDataAccess mappingStoreDataAccess, IMailService mailService)
        {
            try
            { 
                _commonMngr = commonMngr;
                _dbManager = dbManager;
                _mappingStoreDataAccess = mappingStoreDataAccess;
                _mailService = mailService;
                _configuration = configuration;
                _consumerFactory = consumerFactory;
            }
            catch (System.Exception e)
            {
                _log.Error(e.Message);
            }
        }

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            try
            {
                _consumer = _consumerFactory.Create(_configuration, _commonMngr, _dbManager, _mappingStoreDataAccess, _mailService);

                _consumer?.Start();
            }
            catch (System.Exception e)
            {
                _log.Error(e.Message);
            }

            return Task.CompletedTask;
        }

        public override void Dispose()
        {
            _consumer?.Dispose();

            base.Dispose();
        }
    }

}
