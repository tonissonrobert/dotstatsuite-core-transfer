﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Text.Json;
using System.Threading;
using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Logger;
using DotStat.Common.Messaging;
using DotStat.Db.Manager;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStat.Transfer.Utils;
using log4net;
using Microsoft.Extensions.Configuration;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;



[assembly: log4net.Config.XmlConfigurator(ConfigFile = "log4net.config", Watch = true)]

namespace DotStat.Transfer.Messaging.Consumer
{
    public class RabbitMqConsumer : IConsumer 
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(RabbitMqConsumer));

        private readonly IDbManager _dbManager;
        private readonly CommonManager _commonMngr;
        private readonly MessagingServiceConfiguration _messsageBrokerConfiguration;
        private readonly BaseConfiguration _baseConfiguration; 
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly IMailService _mailService;
        private IConnection _connection;
        private IModel _channel;

        public RabbitMqConsumer(IConfiguration configuration, CommonManager commonMngr, IDbManager dbManager, IMappingStoreDataAccess mappingStoreDataAccess, IMailService mailService)
        {
            _commonMngr = commonMngr;
            _dbManager = dbManager;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _mailService = mailService;
            _messsageBrokerConfiguration =  configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            _baseConfiguration = configuration.Get<BaseConfiguration>();
        }
        
        public void Start()
        {
            if (TryCreateConnection(null))
            {
                var consumer = new EventingBasicConsumer(_channel);

                consumer.Received += OnReceived;

                _channel.ModelShutdown += (e, c) =>
                {
                    //_log.Error(c.ReplyText);

                    //TODO: retry to connect
                    throw new System.Exception(c.ReplyText);
                };

                _channel.BasicConsume(_messsageBrokerConfiguration.QueueName, false, consumer);
            }
        }

        /// <summary>
        /// Creates Connection and channel, these are meant to be long-lived, so create them early in the flow.
        /// </summary>
        /// <param name="connectionFactory"></param>
        /// <returns>true if successful connection is created</returns>
        private bool TryCreateConnection(RabbitMQ.Client.IConnectionFactory connectionFactory)
        {
            if (connectionFactory == null)
            {
                connectionFactory = new ConnectionFactory
                {
                    HostName = _messsageBrokerConfiguration.HostName,
                    VirtualHost = _messsageBrokerConfiguration.VirtualHost,
                    Port = _messsageBrokerConfiguration.Port,
                    UserName = _messsageBrokerConfiguration.UserName,
                    Password = _messsageBrokerConfiguration.Password
                };
            }
            
            try
            {
                _connection = connectionFactory.CreateConnection();
                _channel = _connection.CreateModel();

                _channel.QueueDeclare(
                    queue: _messsageBrokerConfiguration.QueueName,
                    durable: _messsageBrokerConfiguration.QueueDurable,
                    exclusive: false,
                    autoDelete: false,
                    arguments: null);

                _connection.ConnectionShutdown += (sender, ea) =>
                {
                    //if messagebroker is unreachable, try to restart 
                    TryCreateConnection(connectionFactory);
                };
            }
            catch (System.Exception e) when (e is BrokerUnreachableException || e is OperationInterruptedException)
            {
                //Do nothing, try again at a later stage
                _log.Warn($"Failed to connect to queue {_messsageBrokerConfiguration.QueueName} on server {_messsageBrokerConfiguration.HostName} because {e.Message}");
                return false;
            }

            return true;
        }


        private void OnReceived(object sender, BasicDeliverEventArgs e)
        {
            if(_connection== null || !_connection.IsOpen || _channel == null || _channel.IsClosed )
                TryCreateConnection(null);

            var queueItem = JsonSerializer.Deserialize<QueueItem>(Encoding.UTF8.GetString(e.Body.ToArray()));
            
            var transactionResult = new TransactionResult()
            {
                TransactionType = TransactionType.Import,
                DestinationDataSpaceId = queueItem.Dataspace,
                User = queueItem.Email
            };

            var managementRepository = _dbManager.GetManagementRepository(queueItem.Dataspace);
            var transactionRepository = _dbManager.GetTransactionRepository(queueItem.Dataspace);

            DotStatPrincipal dotStatPrincipal = new DotStatPrincipal(
                new ClaimsPrincipal(
                    new List<ClaimsIdentity>(new List<ClaimsIdentity>
                    {
                        new ClaimsIdentity(
                            new List<Claim>
                            {
                                new Claim("email", queueItem.Email??"")
                            })
                    })), null);

            var transferParam = new TransferParam
            {
                DestinationDataspace = queueItem.Dataspace.GetSpaceInternal(_baseConfiguration, _baseConfiguration.DefaultLanguageCode),
                Principal = dotStatPrincipal
            };

            var dataflowsMappingStore = _mappingStoreDataAccess.GetDataflows(queueItem.Dataspace);

            var dataflow = queueItem.MaintainableRefObjects.FirstOrDefault();

            if (dataflow == null)
            {
                _channel.BasicAck(e.DeliveryTag, false);
                return;
            }
            
            try
            {
                transferParam.Id = managementRepository.GetNextTransactionId();
                transactionResult.TransactionId = transferParam.Id;
                transactionResult.TransactionStatus = TransactionStatus.InProgress;

                LogHelper.RecordNewTransaction(transferParam.Id,
                    _baseConfiguration.SpacesInternal.First(x => x.Id == queueItem.Dataspace));

                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(queueItem.Dataspace);

                var dataflowMappingStore = dataflowsMappingStore.First(i =>
                    i.AgencyId == dataflow.AgencyId && i.Id == dataflow.MaintainableId &&
                    dataflow.Version == i.Version);

                //No artefacts found in the Mappingstore db, then ignore.
                //E.g. user wasn't authorized to upload artefacts 
                if (dataflowsMappingStore?.Count > 0)
                {
                    transactionResult.Aftefact =
                        $"{dataflowMappingStore.AgencyId}:{dataflowMappingStore.Id}({dataflowMappingStore.Version})";

                    _ = _commonMngr.InitDataDbObjectsOfDataflow(
                        transferParam,
                        dataflowMappingStore.MutableInstance,
                        managementRepository,
                        transactionRepository, CancellationToken.None).Result;
                }

                transactionResult.TransactionStatus = TransactionStatus.Completed;

                _channel.BasicAck(e.DeliveryTag, false);
            }
            catch (System.Exception ex)
            {
                transactionResult.TransactionStatus = TransactionStatus.TimedOut;
                Log.Error(ex);
                _channel.BasicNack(e.DeliveryTag, false, true);
            }
            finally
            {
                _mailService.SendMail(
                    transactionResult,
                    _baseConfiguration.DefaultLanguageCode
                );
            }
        }
        public void Dispose()
        {
            if(_connection?.IsOpen == true)
                _connection.Close(); //Closes connection and all its channelstion.Close(); //Closes connection and all its channels
        }
    }
}
