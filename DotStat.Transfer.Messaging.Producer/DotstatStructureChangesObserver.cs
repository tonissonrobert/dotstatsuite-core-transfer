﻿using Estat.Sdmxsource.Extension.Builder;
using Estat.Sdmxsource.Extension.Model.Error;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Security.Principal;
using System.Threading.Tasks;

namespace DotStat.Transfer.Messaging.Producer
{
    [ExcludeFromCodeCoverage] //No logic to test
    public class DotstatStructureChangesObserver : IStructureChangesObserver, IDisposable
    {
        private readonly IProducer _producer;

        public DotstatStructureChangesObserver(IConfiguration configuration)
        {
            _producer = ProducerFactory.Create(configuration);
        }

        public Task Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal)
        {
            _producer?.Notify(summary, principal);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _producer?.Dispose();
        }
    }
}
