using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Common.Configuration;
using DotStat.Common.Logger;
using DotStat.DB.Repository;
using Microsoft.Extensions.Hosting;

namespace DotStatServices.Transfer.Services
{
    public class TransactionCleanupHostedService : IHostedService
    {
        private readonly string _serviceId;
        private readonly BaseConfiguration _configuration;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;

        public TransactionCleanupHostedService(BaseConfiguration configuration, UnitOfWorkResolver unitOfWorkResolver)
        {
            _configuration = configuration;
            _serviceId = configuration.ServiceId;
            _unitOfWorkResolver = unitOfWorkResolver;
        }

        public async Task StartAsync(CancellationToken stoppingToken)
        {
            //Do not try to close transactions if the config setting ServiceId was not set for this instance.
            if (string.IsNullOrWhiteSpace(_serviceId)) return;

            try
            {
                // The code in here will run when the application starts, and block the startup process until finished
                foreach (var dataSpace in _configuration.SpacesInternal)
                {
                    var unitOfWork = _unitOfWorkResolver(dataSpace.Id);
                    await unitOfWork.TransactionRepository.CancelTransactions(_serviceId);
                }
            }
            catch (Exception ex)
            {
                Log.Fatal(new Exception($"The following unhandled error occurred while trying to cleanup non-closed transactions for the transfer service instance with ServiceId '{_serviceId}'", ex));
            }
        }

        public Task StopAsync(CancellationToken stoppingToken)
        {
            // The code in here will run when the application stops
            // In your case, nothing to do
            return Task.CompletedTask;
        }
    }
}
