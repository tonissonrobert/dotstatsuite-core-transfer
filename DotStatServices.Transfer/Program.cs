﻿using System;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Reflection;
using DotStat.Common.Logger;
using log4net;
using log4net.Config;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Server.Kestrel.Core;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace DotStatServices.Transfer
{
    [ExcludeFromCodeCoverage]
    public class Program
    {
        private static readonly ILog _log = LogManager.GetLogger(typeof(Program));
        private static string binPath;

        public static void Main(string[] args)
        {
            ConfigureLogging();
            CreateWebHostBuilder(args).Build().Run();
        }

        private static void ConfigureLogging()
        {
            var logRepository = LogManager.GetRepository(Assembly.GetEntryAssembly());
            var applicationName = "dotstatsuite-core-transfer";
            GlobalContext.Properties[CustomParameter.Application.ToString()] = applicationName;
            XmlConfigurator.Configure(logRepository, new FileInfo("config/log4net.config"));

            _log.Info($"{applicationName} application configured. Version number: {Assembly.GetExecutingAssembly().GetName().Version}");
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .ConfigureAppConfiguration((hostingContext, config) =>
                {
                    var configDir = new DirectoryInfo("config");

                    if (!configDir.Exists)
                        throw new InvalidOperationException("Config directory not found");

                    Console.WriteLine($"Config directory name {configDir.FullName}");

                    foreach (var json in configDir.GetFiles("*.json"))
                    {
                        Console.WriteLine($"Configuring from `{json.FullName}` file");
                        config.AddJsonFile(json.FullName);
                    }

                    config.AddEnvironmentVariables();
                })
                .ConfigureServices((context, services) =>
                {
                    services.Configure<KestrelServerOptions>(
                        context.Configuration.GetSection("Kestrel"));
                })
                .UseStartup<Startup>();
    }
}
