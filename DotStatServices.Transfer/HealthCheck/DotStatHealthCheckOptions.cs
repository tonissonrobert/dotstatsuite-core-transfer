﻿using System.Diagnostics.CodeAnalysis;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Diagnostics.HealthChecks;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace DotStatServices.Transfer.HealthCheck
{
    /// <summary>
    /// 
    /// </summary>
    [ExcludeFromCodeCoverage]
    public sealed class DotStatHealthCheckOptions : HealthCheckOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public DotStatHealthCheckOptions()
        {
            ResponseWriter = GetResponseWriter;
        }

        private async Task GetResponseWriter(HttpContext httpContext, HealthReport report)
        {
            httpContext.Response.ContentType = MediaTypeNames.Application.Json;

            var json = new JObject();

            foreach (var check in report.Entries)
            {
                json.Add(check.Key, new JObject(
                    new JProperty("status", check.Value.Status.ToString()),
                    new JProperty("details", JObject.FromObject(check.Value.Data)),
                    new JProperty("responseTime", check.Value.Duration.TotalMilliseconds)
                ));
            }

            json.Add(new JProperty("totalResponseTime", report.TotalDuration.TotalMilliseconds));

            await httpContext.Response.WriteAsync(json.ToString(Formatting.Indented)); ;
        }
    }
}
