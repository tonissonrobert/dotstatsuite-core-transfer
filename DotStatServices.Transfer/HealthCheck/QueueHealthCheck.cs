﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Threading;
using System.Threading.Tasks;
using DotStatServices.Transfer.BackgroundJob;
using Microsoft.Extensions.Diagnostics.HealthChecks;

namespace DotStatServices.Transfer.HealthCheck
{
    [ExcludeFromCodeCoverage]
    public class QueueHealthCheck : IHealthCheck
    {
        private readonly BackgroundQueue _backgroundQueue;

        public QueueHealthCheck(BackgroundQueue backgroundQueue)
        {
            _backgroundQueue = backgroundQueue;
        }

        public async Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
        {
            var data = new Dictionary<string, object>()
            {
                { "uptime", DateTime.Now.Subtract(Startup.Start).ToString("c") },
                { "tasksInQueue", _backgroundQueue.TaskInQueue },
                { "runningTasks", _backgroundQueue.RunningTasks },
                { "takenTasks", _backgroundQueue.TakenTasks }
            };
            
            return HealthCheckResult.Healthy(data: data);
        }
    }
}
