﻿using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;
using DotStat.Test;
using DotStat.Test.Moq;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;

namespace DotStat.Transfer.Test.Helper
{
    public sealed class TestProducer<T> : IProducer<T> where T : TransferParam
    {
        private readonly TestMappingStoreDataAccess _msAccess;
        private readonly int _maxObeservations;

        public TestProducer(int maxObservations, string dsdPath = "sdmx/264D_264_SALDI+2.1.xml")
        {
            _msAccess = new TestMappingStoreDataAccess(dsdPath);
            _maxObeservations = maxObservations;
        }

        public Task<IImportReferenceableStructure> GetReferencedStructure(T transferParam, bool throwErrorIfNotFound=true)
        {
            return Task.FromResult((IImportReferenceableStructure)_msAccess.GetDataflow());
        }

        public Task<TransferContent> Process(T transferParam, IImportReferenceableStructure referencedStructure, CancellationToken c)
        {
            var dataflow =  referencedStructure as Dataflow;
            return Task.FromResult( new TransferContent()
            {
                DataObservations =  ObservationGenerator.Generate(dataflow.Dsd, dataflow, true, 2010, 2020, _maxObeservations, cancellationToken:c),
                DatasetAttributes = new List<DataSetAttributeRow>()
            });
        }

        public bool IsAuthorized(T transferParam, IImportReferenceableStructure referencedStructure)
        {
            return true;
        }

        public void Dispose()
        {}
    }
}
