﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using DotStat.Common.Localization;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Util;
using OfficeOpenXml;
using OfficeOpenXml.FormulaParsing.Utilities;

namespace DotStat.Transfer.Excel.Excel
{
    // ReSharper disable once InconsistentNaming
    public class EPPlusExcelDataSource : IExcelDataSource
    {
        private readonly ExcelPackage _ExcelPackage;
        private bool _Modified;
        private readonly FileInfo _OriginalFile;

        public static readonly HashSet<string> ErrorText = new HashSet<string>(StringComparer.Ordinal)
        {
            "#REF!",
            "#DIV/0!",
            "#NULL!",
            "#VALUE!",
            "#NUM!",
            "#N/A",
            "#NAME?",
        };

        //this is to ensure opening in absolute readonly mode
        private readonly FileStream _FileStream;

        public EPPlusExcelDataSource(string workbookFullPath, string templatePath = null, string password = null,
            bool readOnly = true)
        {
            _OriginalFile = workbookFullPath.GetFileInfo();
            if (!_OriginalFile.Exists)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelWorkbookNotLocated),
                    workbookFullPath)
                );
            }

            FileInfo file = _OriginalFile;

            if (templatePath != null)
            {
                readOnly = false;
            }

            if (readOnly)
            {
                _FileStream = new FileStream(file.FullName, FileMode.Open, FileAccess.Read, FileShare.ReadWrite);
                _ExcelPackage = new ExcelPackage(_FileStream);
            }
            else
            {
                _FileStream = null;
                _ExcelPackage = templatePath == null
                    ? new ExcelPackage(file)
                    : new ExcelPackage(file, templatePath.GetFileInfo());
            }
            if (password != null)
            {
                _ExcelPackage.Workbook.Protection.SetPassword(password);
            }

            // Hack for anomaly in EPPlus - See Redmine #6984
            ExcelWorksheets worksheets;
            try
            {
                worksheets = _ExcelPackage.Workbook.Worksheets;
            }
            catch (System.Exception)
            {
                // Swallow silently an exception and retry
                worksheets = _ExcelPackage.Workbook.Worksheets;
            }
            if (worksheets.Count == 0)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelNoWorkSheets),
                    workbookFullPath)
                );
            }

            CurrentWorksheet = worksheets.First().Name;
        }

        public string Name
        {
            get { return _OriginalFile.FullName; }
        }

        public string GetCellValue(string cellAddress, string sheetName = null)
        {
            var sheet = CellReference.DetermineSheetName(ref cellAddress, sheetName) ?? CurrentWorksheet;
            if (cellAddress == null || sheet == null)
            {
                return null;
            }
            var vs = _ExcelPackage.Workbook.Worksheets[sheet];
            if (vs == null)
            {
                return null;
            }
            var cell = vs.Cells[cellAddress];
            return GetCellValue(cell);
        }

        public string GetCellValue(int row, int column, string sheet = null)
        {
            var vs = _ExcelPackage.Workbook.Worksheets[sheet ?? CurrentWorksheet];
            if (vs == null)
            {
                return null;
            }
            var cell = vs.Cells[row, column];
            return GetCellValue(cell);
        }

        private static string GetCellValue(ExcelRange cell)
        {
            if (cell == null || cell.Value == null)
            {
                return null;
            }
            if (cell.Value.IsNumeric())
            {
                var num = Convert.ToDouble(cell.Value);
                return num.ToString("0.##############");
            }
            if (cell.Value is DateTime)
            {
                var date = Convert.ToDateTime(cell.Value);
                return date.ToString("O");
            }
            var res = cell.GetValue<string>();

            if (string.IsNullOrWhiteSpace(res))
            {
                return null;
            }
            return res.Trim();
        }

        public string GetFormattedCellValue(int row, int column, string sheetName = null)
        {
            //todo: update this once EPPlus supports formatted text
            return GetCellValue(row, column, sheetName);
        }

        public string GetFormattedCellValue(string cellAddress, string sheetName = null)
        {
            //todo: update this once EPPlus supports formatted text
            return GetCellValue(cellAddress, sheetName);
        }

        public string GetCellComment(string cellAddress, string sheetName = null)
        {
            var sheet = CellReference.DetermineSheetName(ref cellAddress, sheetName) ?? CurrentWorksheet;
            if (cellAddress == null || sheet == null)
            {
                return null;
            }
            var vs = _ExcelPackage.Workbook.Worksheets[sheet];
            if (vs == null)
            {
                return null;
            }
            var cell = vs.Cells[cellAddress];
            return GetCellComment(cell);
        }

        public string GetCellComment(int row, int column, string sheet = null)
        {
            var vs = _ExcelPackage.Workbook.Worksheets[sheet ?? CurrentWorksheet];
            if (vs == null)
            {
                return null;
            }
            var cell = vs.Cells[row, column];
            return GetCellComment(cell);
        }

        private static string GetCellComment(ExcelRange cell)
        {
            if (cell == null || cell.Comment == null)
            {
                return null;
            }
            return cell.Comment.Text;
        }

        public void SetCellValue(object value, string cellAddress, string sheetName = null)
        {
            var sheet = CellReference.DetermineSheetName(ref cellAddress, sheetName) ?? CurrentWorksheet;
            if (cellAddress == null || sheet == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelCellNotLocated),
                    sheet, cellAddress,
                    WorkbookName)
                );
            }
            var vs = _ExcelPackage.Workbook.Worksheets[sheet];
            if (vs == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelSheetNotLocated),
                    sheet, WorkbookName)
                );
            }
            var cell = vs.Cells[cellAddress];
            if (cell == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelCellNotLocated),
                    sheet, cellAddress,
                    WorkbookName)
                );
            }

            cell.Value = value;
            _Modified = true;
        }

        public void SetFormattedCellValue(object value, string cellAddress, string sheetName = null)
        {
            //todo: update this once EPPlus supports formatted text
            SetCellValue(value, cellAddress, sheetName);
        }

        public void SetCellComment(string comment, string cellAddress, string sheetName = null)
        {
            var sheet = CellReference.DetermineSheetName(ref cellAddress, sheetName) ?? CurrentWorksheet;
            if (cellAddress == null || sheet == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelCellNotLocated),
                    sheet, cellAddress,
                    WorkbookName)
                );
            }
            var vs = _ExcelPackage.Workbook.Worksheets[sheet];
            if (vs == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelSheetNotLocated),
                    sheet, WorkbookName)
                );
            }
            var cell = vs.Cells[cellAddress];
            if (cell == null)
            {
                throw new ApplicationArgumentException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ExcelCellNotLocated),
                    sheet, cellAddress,
                    WorkbookName)
                );
            }

            cell.AddComment(comment, null);
            _Modified = true;
        }

        public IEnumerable<string> Worksheets
        {
            get { return _ExcelPackage.Workbook.Worksheets.Select(ws => ws.Name); }
        }

        public string CurrentWorksheet { get; set; }

        public string WorkbookName
        {
            get { return Name; }
        }

        public int GetRowCount(string sheetName = null)
        {
            var vs = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet];
            if (vs == null)
            {
                return 0;
            }
            return vs.Cells.MaxOrDefault(c => c.Start.Row);
        }

        public int GetColumnCount(string sheetName = null)
        {
            var vs = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet];
            if (vs == null)
            {
                return 0;
            }
            return vs.Cells.MaxOrDefault(c => c.Start.Column);
        }

        public bool IsCellValueValid(int row, int column, string sheetName = null)
        {
            return
                !ErrorText.Contains(
                    _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Cells[row, column].Text);
        }

        public IEnumerable<Point> FindCells(string sheetName,
            string range,
            string pattern,
            bool isRegex = false,
            bool nonMatching = false)
        {
            var cells = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Cells[range];
            var erow = cells.End.Row;
            var ecol = cells.End.Column;
            var scol = cells.Start.Column;
            if (pattern == null)
            {
                for (var row = cells.Start.Row; row <= erow; row++)
                {
                    for (var column = scol; column <= ecol; column++)
                    {
                        yield return new Point(column, row);
                    }
                }
            }
            else
            {
                pattern = pattern.Trim();
                var regex = !isRegex
                    ? null
                    : new Regex(pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                for (var row = cells.Start.Row; row <= erow; row++)
                {
                    for (var column = scol; column <= ecol; column++)
                    {
                        var cell = cells[row, column];
                        var val = GetCellValue(cell) ?? string.Empty;
                        var mr = regex == null ? val.IsSameAs(pattern) : regex.IsMatch(val);
                        if (nonMatching ? !mr : mr)
                        {
                            yield return new Point(column, row);
                        }
                    }
                }
            }
        }

        public IEnumerable<int> FindRows(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            var cells = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Cells[range];
            int erow = cells.End.Row;
            if (pattern == null)
            {
                for (var row = cells.Start.Row; row <= erow; row++)
                {
                    yield return row;
                }
            }
            else
            {
                var scol = cells.Start.Column;
                var ecol = cells.End.Column;
                pattern = pattern.Trim();
                var regex = !isRegex
                    ? null
                    : new Regex(pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                for (var row = cells.Start.Row; row <= erow; row++)
                {
                    for (var column = scol; column <= ecol; column++)
                    {
                        var cell = cells[row, column];
                        var val = GetCellValue(cell) ?? string.Empty;
                        var mr = regex == null ? val.IsSameAs(pattern) : regex.IsMatch(val);
                        if (nonMatching ? !mr : mr)
                        {
                            yield return row;
                        }
                    }
                }
            }
        }

        public IEnumerable<int> FindColumns(string sheetName,
            string range,
            string pattern,
            bool isRegex = false,
            bool nonMatching = false)
        {
            var cells = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Cells[range];
            int ecol = cells.End.Column;
            if (pattern == null)
            {
                for (int column = cells.Start.Column; column <= ecol; column++)
                {
                    yield return column;
                }
            }
            else
            {
                int erow = cells.End.Row;
                pattern = pattern.Trim();
                var regex = !isRegex
                    ? null
                    : new Regex(pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                for (int column = cells.Start.Column; column <= ecol; column++)
                {
                    for (int row = cells.Start.Row; row <= erow; row++)
                    {
                        var cell = cells[row, column];
                        var val = GetCellValue(cell) ?? string.Empty;
                        var mr = regex == null ? val.IsSameAs(pattern) : regex.IsMatch(val);
                        if (nonMatching ? !mr : mr)
                        {
                            yield return column;
                        }
                    }
                }
            }
        }

        public Point? FindFirstCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            var res = FindCells(sheetName, range, pattern, isRegex, nonMatching).FirstOrDefault();
            if (res.X == 0)
            {
                return null;
            }
            return res;
        }

        public Point? FindLastCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            var sheet = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet];
            if (sheet == null)
            {
                return null;
            }
            var cells = sheet.Cells[range];
            if (pattern == null)
            {
                return new Point(cells.End.Column, cells.End.Row);
            }
            var brow = cells.Start.Row;
            var erow = cells.End.Row;
            var bcol = cells.Start.Column;
            var ecol = cells.End.Column;
            pattern = pattern.Trim();
            var regex = !isRegex
                ? null
                : new Regex(pattern, RegexOptions.Compiled | RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
            for (int row = erow; row >= brow; row--)
            {
                for (int column = ecol; column >= bcol; column--)
                {
                    var cell = cells[row, column];
                    var val = GetCellValue(cell) ?? string.Empty;
                    var mr = regex == null ? val.IsSameAs(pattern) : regex.IsMatch(val);
                    if (nonMatching ? !mr : mr)
                    {
                        return new Point(column, row);
                    }
                }
            }
            return null;
        }

        public int LastCellRow(string sheetName = null)
        {
            var dim = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Dimension;
            if (dim == null)
            {
                return 0;
            }
            return dim.End.Row;
        }

        public int LastCellColumn(string sheetName = null)
        {
            var dim = _ExcelPackage.Workbook.Worksheets[sheetName ?? CurrentWorksheet].Dimension;
            if (dim == null)
            {
                return 0;
            }
            return dim.End.Column;
        }

        public void Save()
        {
            if (!_Modified)
            {
                return;
            }
            _ExcelPackage.Save();
            _Modified = false;
        }

        public void Dispose()
        {
            Save();
            _ExcelPackage.Dispose();
            if (_FileStream != null)
            {
                _FileStream.Dispose();
            }
        }
    }
}