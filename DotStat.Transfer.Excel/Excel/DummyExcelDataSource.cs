﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Drawing;
using System.Linq;

namespace DotStat.Transfer.Excel.Excel
{
    [ExcludeFromCodeCoverage]
    public class DummyExcelDataSource : IExcelDataSource
    {
        //todo: make this based on a sparse matrix, will be useful for testing
        public readonly ExcelCoordMappingDescriptor Owner;
        private readonly string _WorkbookName;
        public string CellValue;
        public string CellComment;

        public DummyExcelDataSource(ExcelCoordMappingDescriptor owner, string workbookName)
        {
            Owner = owner;
            _WorkbookName = workbookName;
            Name = owner.Name;
            CellValue = "0";
            CellComment = "";
            _CurrentSheetColumnCount = 1;
            _CurrentSheetRowCount = 1;
            CurrentWorksheet = "Dummy";
        }

        public void Dispose()
        {
            //noop;
        }

        public string Name { get; private set; }

        public string GetCellValue(string cellAddress, string sheetName = null)
        {
            return CellValue;
        }

        public string GetCellValue(int row, int column, string sheetName = null)
        {
            return CellValue;
        }

        public string GetFormattedCellValue(int row, int column, string sheetName = null)
        {
            return CellValue;
        }

        public string GetFormattedCellValue(string cellAddress, string sheetName = null)
        {
            return CellValue;
        }

        public string GetCellComment(string cellAddress, string sheetName = null)
        {
            return CellComment;
        }

        public string GetCellComment(int row, int column, string sheetName)
        {
            return CellComment;
        }

        public void SetCellValue(object value, string cellAddress, string sheetName = null)
        {
            CellValue = value == null ? "" : value.ToString();
        }

        public void SetFormattedCellValue(object value, string cellAddress, string sheetName = null)
        {
            SetCellValue(value, cellAddress, sheetName);
        }

        public void SetCellComment(string comment, string cellAddress, string sheetName = null)
        {
            CellComment = comment ?? "";
        }

        public IEnumerable<string> Worksheets
        {
            get { return Owner.Worksheets.Select(ws => ws.Pattern); }
        }

        public string CurrentWorksheet { get; set; }

        public string WorkbookName
        {
            get { return _WorkbookName; }
        }

        private int _CurrentSheetColumnCount;
        private int _CurrentSheetRowCount;

        public int GetRowCount(string sheetName = null)
        {
            return _CurrentSheetRowCount;
        }

        public int GetColumnCount(string sheetName = null)
        {
            return _CurrentSheetColumnCount;
        }

        public bool IsCellValueValid(int row, int column, string sheetName = null)
        {
            return true;
        }

        public IEnumerable<int> FindColumns(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            return new[] {1};
        }

        public IEnumerable<int> FindRows(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            return new[] {1};
        }


        public IEnumerable<Point> FindCells(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            return new[] {new Point(1, 1)};
        }

        public Point? FindFirstCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            return new Point(1, 1);
        }

        public Point? FindLastCell(string sheetName, string range, string pattern, bool isRegex = false,
            bool nonMatching = false)
        {
            return new Point(1, 1);
        }

        public int LastCellRow(string sheetName)
        {
            return 1;
        }

        public int LastCellColumn(string sheetName)
        {
            return 1;
        }

        public void Save()
        {
            //noop;
        }
    }
}